package com.example.administrator.jim.view.main;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.administrator.jim.BaseActivity;
import com.example.administrator.jim.MainActivity;
import com.example.administrator.jim.R;
import com.example.administrator.jim.utils.BitmapUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.jpush.im.android.api.callback.GetAvatarBitmapCallback;

/**
 * Created by Administrator on 2018/2/6 0006.
 * Date 2018/2/6 0006
 */

public class MyTwoCodeActivity extends BaseActivity {
    @BindView(R.id.activity_twocode_img_autor)
    ImageView mImageView_avator;
    @BindView(R.id.activity_twocode_img_twocode)
    ImageView mImageView_twocode;
    @BindView(R.id.activity_twocode_img_back)
    ImageView mImageView_back;
    @BindView(R.id.activity_twocode_tv_username)
    TextView mTextView_username;
    @BindView(R.id.activity_twocode_tv_address)
    TextView mTextView_address;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_twocode);
        setClazz(MyTwoCodeActivity.class);
        getSupportActionBar().hide();
        ButterKnife.bind(this);
        initDatas();
    }
    @OnClick(R.id.activity_twocode_img_back)
    public void onClick(View v) {
        finish();
    }
    private void initDatas() {
        String userName = MainActivity.mMyInfo.getUserName();
        String nickname = MainActivity.mMyInfo.getNickname();
        String address = MainActivity.mMyInfo.getAddress();
        if(nickname.equals("")){
            mTextView_username.setText(userName);
        }else {
            mTextView_username.setText(nickname);
        }
        mTextView_address.setText(address);
        Bitmap bitmap = BitmapUtils.encodeAsBitmap(userName);
        if(bitmap!=null){
            mImageView_twocode.setImageBitmap(bitmap);
        }
        MainActivity.mMyInfo.getAvatarBitmap(new GetAvatarBitmapCallback() {
            @Override
            public void gotResult(int i, String s, Bitmap bitmap) {
                if(bitmap!=null){
                    mImageView_avator.setImageBitmap(bitmap);
                }
            }
        });
    }
}
