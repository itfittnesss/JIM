package com.example.administrator.jim.share;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.widget.Toast;

import com.example.administrator.jim.R;
import com.sdsmdg.tastytoast.TastyToast;

import me.shaohui.bottomdialog.BaseBottomDialog;
import me.shaohui.shareutil.ShareUtil;
import me.shaohui.shareutil.share.ShareListener;
import me.shaohui.shareutil.share.SharePlatform;

/**
 * Created by shaohui on 2016/12/10.
 */

public class ShareBottomDialog extends BaseBottomDialog implements View.OnClickListener {

    private ShareListener mShareListener;

    @Override
    public int getLayoutRes() {
        return R.layout.layout_bottom_share;
    }

    @Override
    public void bindView(final View v) {
        v.findViewById(R.id.share_qq).setOnClickListener(this);
        v.findViewById(R.id.share_qzone).setOnClickListener(this);
        v.findViewById(R.id.share_weibo).setOnClickListener(this);
        v.findViewById(R.id.share_wx).setOnClickListener(this);
        v.findViewById(R.id.share_wx_timeline).setOnClickListener(this);

        mShareListener = new ShareListener() {
            @Override
            public void shareSuccess() {
                TastyToast.makeText(v.getContext(), "分享成功!", TastyToast.LENGTH_SHORT, TastyToast.SUCCESS);
            }

            @Override
            public void shareFailure(Exception e) {
                TastyToast.makeText(v.getContext(), "分享失败!", TastyToast.LENGTH_SHORT, TastyToast.ERROR);
            }

            @Override
            public void shareCancel() {
                TastyToast.makeText(v.getContext(), "取消分享!", TastyToast.LENGTH_SHORT, TastyToast.INFO);

            }
        };
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.share_qq:
                Bitmap b= BitmapFactory.decodeResource(getResources(),R.mipmap.logo);
                ShareUtil.shareImage(getContext(), SharePlatform.QQ,
                        b, mShareListener);
                break;
            case R.id.share_qzone:
                ShareUtil.shareMedia(getContext(), SharePlatform.QZONE, "DChat", "DChat是一款专门为现代人打造的简洁好用的聊天工具",
                        "http://www.google.com", "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1514221742981&di=cd6d5a58925f04b6801987ab9c8b738e&imgtype=0&src=http%3A%2F%2Fimgsrc.baidu.com%2Fimgad%2Fpic%2Fitem%2Fc83d70cf3bc79f3d9b8985f9b0a1cd11738b298a.jpg",
                        mShareListener);
                break;
            case R.id.share_weibo:
                Toast.makeText(getContext(), "暂未开通", Toast.LENGTH_SHORT).show();
//                ShareUtil.shareImage(getContext(), SharePlatform.WEIBO,
//                        "http://shaohui.me/images/avatar.gif", mShareListener);
                break;
            case R.id.share_wx_timeline:
                Toast.makeText(getContext(), "暂未开通", Toast.LENGTH_SHORT).show();
//                ShareUtil.shareText(getContext(), SharePlatform.WX_TIMELINE, "测试分享文字",
//                        mShareListener);
                break;
            case R.id.share_wx:
                Toast.makeText(getContext(), "暂未开通", Toast.LENGTH_SHORT).show();
//                ShareUtil.shareMedia(getContext(), SharePlatform.WX, "Title", "summary",
//                        "http://www.google.com", "http://shaohui.me/images/avatar.gif",
//                        mShareListener);
                break;
        }
        dismiss();
    }
}
