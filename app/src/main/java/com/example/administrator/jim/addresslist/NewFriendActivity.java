package com.example.administrator.jim.addresslist;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.example.administrator.jim.BaseActivity;
import com.example.administrator.jim.R;
import com.example.administrator.jim.adapter.NewFriendAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.example.administrator.jim.addresslist.AddressListView.mdatas;

/**
 * Created by Administrator on 2017/12/20 0020.
 * Date 2017/12/20 0020
 */

public class NewFriendActivity extends BaseActivity {
    @BindView(R.id.newfriendactivity_ImgView_back)
    ImageView mNewfriendactivityImgViewBack;
    @BindView(R.id.newfriendactivity_RecyclerView)
    RecyclerView mNewfriendactivityRecyclerView;
    private NewFriendAdapter mNewFriendAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newfriend);
        setClazz(NewFriendActivity.class);
        getSupportActionBar().hide();
        ButterKnife.bind(this);
        initDatas();
    }
    //显示验证信息列表
    private void initDatas() {
        mNewfriendactivityRecyclerView.setLayoutManager(new LinearLayoutManager(NewFriendActivity.this));
        mNewFriendAdapter = new NewFriendAdapter(NewFriendActivity.this, mdatas, R.layout.item_newfriend);
        mNewfriendactivityRecyclerView.setAdapter(mNewFriendAdapter);
    }
    @OnClick(R.id.newfriendactivity_ImgView_back)
    public void onClick(View v){
        finish();
    }

}
