package com.example.administrator.jim.bean;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import cn.jpush.im.android.api.model.UserInfo;

/**
 * Created by gjz on 9/3/16.
 */
public class Contact implements Comparable{
    private String index;
    private String name;
    private String username;
    private Bitmap mBitmap;
    private UserInfo mUserInfo;

    public UserInfo getUserInfo() {
        return mUserInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        mUserInfo = userInfo;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Bitmap getBitmap() {
        return mBitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        mBitmap = bitmap;
    }
    private int messageflag=0;
    public Contact(String index, String name) {
        this.index = index;
        this.name = name;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public int getMessageflag() {
        return messageflag;
    }

    public void setMessageflag(int messageflag) {
        this.messageflag = messageflag;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public static List<Contact> getTopTwoContacts() {
        List<Contact> contacts = new ArrayList<>();
        contacts.add(new Contact("A", "验证消息"));
        contacts.add(new Contact("A", "群组"));
        return contacts;
    }

    @Override
    public int compareTo(@NonNull Object o) {
        if(o instanceof Contact){
            Contact emp = (Contact) o;
            return this.index.compareTo(emp.getIndex());//排序
        }
        throw new ClassCastException("不能转换为Contact类型的对象...");
    }
}
