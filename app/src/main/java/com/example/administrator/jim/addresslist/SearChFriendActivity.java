package com.example.administrator.jim.addresslist;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.administrator.jim.BaseActivity;
import com.example.administrator.jim.R;
import com.example.administrator.jim.adapter.SearchFriendAdapter;
import com.example.administrator.jim.bean.Contact;
import com.example.administrator.jim.imuisample.messages.MessageListActivity;
import com.example.administrator.jim.minterface.SearchFriendListClickListener;
import com.example.administrator.jim.value.MyValue;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Administrator on 2018/2/7 0007.
 * Date 2018/2/7 0007
 */

public class SearChFriendActivity extends BaseActivity {
    @BindView(R.id.searchfriendactivity_title_mine_huitui)
    ImageView mImageView_huitui;
    @BindView(R.id.searchfriendactivity_search)
    TextView mTextView_search;
    @BindView(R.id.searchfriendactivity_et_search)
    EditText mEditText_search;
    @BindView(R.id.searchfriendactivity_contacts)
    RecyclerView mRecyclerView;
    @BindView(R.id.addfriendactivity_title_textview)
    TextView mTextView_title;
    private SearchFriendAdapter mSearchFriendAdapter;
    private int OldCount=0;
    private Intent mIntent;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setClazz(SearChFriendActivity.class);
        setContentView(R.layout.activity_searchfriend);
        getSupportActionBar().hide();
        ButterKnife.bind(this);
        mIntent=new Intent(this, MessageListActivity.class);
        initView();
        initListener();
    }

    private void initView() {
        String stringExtra = getIntent().getStringExtra(MyValue.SEARCHFRIEND_TITLE);
        if(stringExtra!=null){
            mTextView_title.setText(stringExtra);
        }
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    private void initListener() {
        mEditText_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(start>=2){
                    getDatas(s.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    private void getDatas(String s) {
        final List<Contact> list=new ArrayList<>();
        for(int x=2;x<AddressListView.contacts.size();x++){
            Contact c = AddressListView.contacts.get(x);
            if(c.getUsername().contains(s)||c.getName().contains(s)){
                list.add(c);
            }
        }
        if(list.size()!=OldCount){
            if(mSearchFriendAdapter!=null){
                mSearchFriendAdapter.setContacts(list);
                mSearchFriendAdapter.notifyDataSetChanged();
            }else {
                mSearchFriendAdapter=new SearchFriendAdapter(this,list,R.layout.item_contacts);
                mSearchFriendAdapter.setSearchFriendListClickListener(new SearchFriendListClickListener() {
                    @Override
                    public void onItemClick(View v, int pos) {
                        mIntent.putExtra("username",list.get(pos).getUsername());
                        startActivity(mIntent);
                    }
                });
                mRecyclerView.setAdapter(mSearchFriendAdapter);
            }
        }
        OldCount=list.size();
    }
    @OnClick({R.id.searchfriendactivity_title_mine_huitui,R.id.searchfriendactivity_search})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.searchfriendactivity_search:
                getDatas(mEditText_search.getText().toString().trim());
                break;
            case R.id.searchfriendactivity_title_mine_huitui:
                finish();
                break;
        }
    }
}
