package com.example.administrator.jim.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.administrator.jim.R;

/**
 * Created by BarBrothers on 2017/12/16.
 */

public class Mine_ViewSetting_ListViewAdapter extends BaseAdapter {
    private int imagedatas[];
    private String txtdatas[];
    private String desc[];
    private Context mContext;
    public Mine_ViewSetting_ListViewAdapter(Context ctx, int imagedatas[], String txtdatas[],String desc[]){
        mContext=ctx;
        this.imagedatas=imagedatas;
        this.txtdatas=txtdatas;
        this.desc=desc;
    }
    @Override
    public int getCount() {
        return imagedatas.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View inflate = View.inflate(mContext, R.layout.item_minesetting, null);
        ImageView img= (ImageView) inflate.findViewById(R.id.item_minesetting_icon);
        TextView tv_title= (TextView) inflate.findViewById(R.id.item_minesetting_text);
        TextView tv_desc= (TextView) inflate.findViewById(R.id.item_minesetting_desc);
        img.setImageResource(imagedatas[i]);
        tv_title.setText(txtdatas[i]);
        tv_desc.setText(desc[i]);
        return inflate;
    }
}
