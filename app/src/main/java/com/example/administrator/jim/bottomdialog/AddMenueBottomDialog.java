package com.example.administrator.jim.bottomdialog;

import android.content.Intent;
import android.view.View;

import com.example.administrator.jim.R;
import com.example.administrator.jim.addresslist.AddFriendActivity;
import com.example.administrator.jim.addresslist.CustomScanActivity;
import com.example.administrator.jim.addresslist.SearChFriendActivity;
import com.example.administrator.jim.value.MyValue;
import com.google.zxing.integration.android.IntentIntegrator;
import com.sdsmdg.tastytoast.TastyToast;

import me.shaohui.bottomdialog.BaseBottomDialog;

/**
 * Created by shaohui on 2016/12/10.
 */

public class AddMenueBottomDialog extends BaseBottomDialog implements View.OnClickListener{
    @Override
    public int getLayoutRes() {
        return R.layout.layout_bottom_addfmenue;
    }
    @Override
    public void bindView(final View v) {
        v.findViewById(R.id.layout_bottom_addmenue_qunliao).setOnClickListener(this);
        v.findViewById(R.id.layout_bottom_gender_addmenue_danliao).setOnClickListener(this);
        v.findViewById(R.id.layout_bottom_gender_addmenue_addfriend).setOnClickListener(this);
        v.findViewById(R.id.layout_bottom_gender_addmenue_sys).setOnClickListener(this);
        v.findViewById(R.id.layout_bottom_gender_addmenue_calc).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.layout_bottom_addmenue_qunliao:
                TastyToast.makeText(getContext(), "暂未开通", TastyToast.LENGTH_SHORT, TastyToast.INFO);
                break;
            case R.id.layout_bottom_gender_addmenue_danliao:
                Intent intent = new Intent(getContext(), SearChFriendActivity.class);
                intent.putExtra(MyValue.SEARCHFRIEND_TITLE,"发起单聊");
                startActivity(intent);
                break;
            case R.id.layout_bottom_gender_addmenue_addfriend:
                startActivity(new Intent(getContext(), AddFriendActivity.class));
                break;
            case R.id.layout_bottom_gender_addmenue_sys:
                new IntentIntegrator(getActivity())
                        .setOrientationLocked(false)
                        .setCaptureActivity(CustomScanActivity.class) // 设置自定义的activity是CustomActivity
                        .initiateScan(); // 初始化扫描
                break;
        }
        dismiss();
    }
}
