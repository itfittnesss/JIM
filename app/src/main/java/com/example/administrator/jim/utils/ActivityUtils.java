package com.example.administrator.jim.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;


import java.util.List;

/**
 * Created by BarBrothers on 2017/12/30.
 */

public class ActivityUtils {
    public static boolean isActivityTop(Context context,Class clazz){
        ActivityManager manager = (ActivityManager) context.getSystemService(Activity.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> runningTaskInfos = manager.getRunningTasks(1);
        String className="";
        if (runningTaskInfos != null && !runningTaskInfos.isEmpty()) {
            className = runningTaskInfos.get(0).topActivity.getClassName();
        }
        return className.equals(clazz.getName());
    }
}
