package com.example.administrator.jim.bottomdialog;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.administrator.jim.MainActivity;
import com.example.administrator.jim.R;
import com.example.administrator.jim.view.main.Mine_SettingActivity;
import com.sdsmdg.tastytoast.TastyToast;

import cn.jpush.im.android.api.JMessageClient;
import cn.jpush.im.android.api.model.UserInfo;
import cn.jpush.im.api.BasicCallback;
import me.shaohui.bottomdialog.BaseBottomDialog;

/**
 * Created by shaohui on 2016/12/10.
 */

public class NiknameSettingBottomDialog extends BaseBottomDialog implements View.OnClickListener{
    EditText mEditText;
    TextView mTextView;
    @Override
    public int getLayoutRes() {
        return R.layout.layout_bottom_nikname_setting;
    }
    @Override
    public void bindView(final View v) {
        mEditText=(EditText)v.findViewById(R.id.edit_text);
        mTextView=(TextView)v.findViewById(R.id.TextView_send);
        v.findViewById(R.id.TextView_send_quxiao).setOnClickListener(this);
        mTextView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.TextView_send:
                MainActivity.mMyInfo.setNickname(mEditText.getText().toString().trim());
                JMessageClient.updateMyInfo(UserInfo.Field.nickname, MainActivity.mMyInfo, new BasicCallback() {
                    @Override
                    public void gotResult(int i, String s) {
                    }
                });
                TastyToast.makeText(getContext(), "修改成功!", TastyToast.LENGTH_SHORT, TastyToast.SUCCESS);
                //调用Activity的更新用户数据方法
                ((Mine_SettingActivity)getContext()).updateUserInfo();
                dismiss();
                break;
            case R.id.TextView_send_quxiao:
                dismiss();
                break;
        }
    }
}
