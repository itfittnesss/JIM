package com.example.administrator.jim.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.widget.Toast;

import com.example.administrator.jim.R;
import com.example.administrator.jim.view.main.Mine_SettingActivity;
import com.sdsmdg.tastytoast.TastyToast;

import net.lemonsoft.lemonbubble.LemonBubble;
import net.lemonsoft.lemonbubble.enums.LemonBubbleLayoutStyle;
import net.lemonsoft.lemonbubble.enums.LemonBubbleLocationStyle;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import agency.tango.android.avatarview.views.AvatarView;
import cn.jpush.im.android.api.JMessageClient;
import cn.jpush.im.api.BasicCallback;
import me.weyye.hipermission.HiPermission;
import me.weyye.hipermission.PermissionCallback;
import me.weyye.hipermission.PermissionItem;


/**
 * Created by Administrator on 2017/12/11 0011.
 * Date 2017/12/11 0011
 */

public class CameraUtils {
    public static final int PHOTO_REQUEST_GALLERY = 100;
    private boolean flag=false;
    public static final int PHOTO_REQUEST_CAMERA = 101;
    public static final int PHOTO_REQUEST_CUT = 102;
    private File tempFile;
    private static final String PHOTO_FILE_NAME = "temp_photo.jpg";
    //上传时文件名
    public static final String USER_ICON_NAME = "user_icon.jpg";
    //上传时文件路径
    public static final String CACHE_DIR = "user_icon";
    private Context mContext;
    private ByteArrayOutputStream mStream;
    private Bitmap mBitmap;
    public CameraUtils(Context ctx){
        mContext=ctx;
    }
    //检测权限
    private Handler mHandler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            File file = (File) msg.obj;
            commit(file);
            LemonBubble.getRoundProgressBubbleInfo()
                    .setLocationStyle(LemonBubbleLocationStyle.BOTTOM)
                    .setLayoutStyle(LemonBubbleLayoutStyle.ICON_LEFT_TITLE_RIGHT)
                    .setBubbleSize(200, 50)
                    .setProportionOfDeviation(0.1f)
                    .setTitle("正在上传头像...")
                    .show(mContext);
        }
    };
    public void checkpermissiom(){
        List<PermissionItem> permissionItems = new ArrayList<PermissionItem>();
        permissionItems.add(new PermissionItem(Manifest.permission.CAMERA, "打开相机", R.drawable.permission_ic_camera));
        permissionItems.add(new PermissionItem(Manifest.permission.WRITE_EXTERNAL_STORAGE, "读写内存卡", R.drawable.permission_ic_storage));
        HiPermission.create(mContext)
                .title("亲爱的上帝")
                .permissions(permissionItems)
                .msg("为了保护世界的和平,开启这些权限吧！\n你我一起拯救世界！")
                .animStyle(R.style.PermissionAnimScale)
                .style(R.style.PermissionDefaultBlueStyle)
                .checkMutiPermission(new PermissionCallback() {
                    @Override
                    public void onClose() {
                        Toast.makeText(mContext, "用户关闭权限申请", Toast.LENGTH_SHORT).show();
                    }
                    @Override
                    public void onFinish() {
                        flag=true;
                    }
                    @Override
                    public void onDeny(String permission, int position) {
                    }
                    @Override
                    public void onGuarantee(String permission, int position) {
                    }
                });
    }
    // 打开相册
    public void openGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        // 开启一个带有返回值的Activity，请求码为PHOTO_REQUEST_GALLERY
        ((Activity)mContext).startActivityForResult(intent, PHOTO_REQUEST_GALLERY);
    }
    /* 判断sdcard是否被挂载
*/
    private boolean hasSdcard() {
        if (Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            return true;
        } else {
            return false;
        }
    }
    //将图片加载到ImageView
    public void initImage(int requestCode, Intent data, AvatarView img){
        if (requestCode == PHOTO_REQUEST_GALLERY) {
            if (data != null) {
                Uri uri = data.getData();
                crop(uri);
            }
        } else if (requestCode == PHOTO_REQUEST_CAMERA) {
            // 从相机返回的数据
            // 裁剪
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                //  大于等于24即为7.0及以上执行内容
                crop(FileProvider.getUriForFile(mContext, "com.example.administrator.jim", tempFile));
            } else {
                //  低于24即为7.0以下执行内容
                crop(Uri.fromFile(tempFile));
            }
        } else if (requestCode == PHOTO_REQUEST_CUT) {// 来自裁剪图片
            if (data != null) {
                mStream = new ByteArrayOutputStream();
                mBitmap = data.getParcelableExtra("data");
                mBitmap.compress(Bitmap.CompressFormat.PNG, 100, mStream);
//                Glide.with(mContext).load(mStream.toByteArray()).into(img);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        creatFile(mBitmap);
                    }
                }).start();
            }
        }
    }
    public File getFileForSendMessage(int requestCode){
        if (requestCode == PHOTO_REQUEST_CAMERA) {
            return tempFile;
        }else {
            return null;
        }
    }
    private void commit(File file) {
        JMessageClient.updateUserAvatar(file, new BasicCallback() {
            @Override
            public void gotResult(int i, String s) {
                if(i==0){
                    ((Mine_SettingActivity)mContext).updateUserInfo();
                    LemonBubble.hide();
                    TastyToast.makeText(mContext, "头像修改成功!", TastyToast.LENGTH_SHORT, TastyToast.SUCCESS);
                }else {
                    LemonBubble.hide();
                    TastyToast.makeText(mContext, "头像修改失败!", TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                }

            }
        });
    }
    private void creatFile(Bitmap bitmap) {
        File cache = new File(mContext.getCacheDir() + File.separator + CACHE_DIR);
        BufferedOutputStream bos = null;
        File file = null;
        if (!cache.exists()) {
            cache.mkdirs();
        }
        try {
            file = new File(cache, USER_ICON_NAME);
            bos = new BufferedOutputStream(new FileOutputStream(file));
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
            bos.flush();
            bos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Message message=Message.obtain();
        message.obj=file;
        mHandler.sendMessage(message);
    }
    /*
* 剪切图片
*/
    private void crop(Uri uri) {
        // 裁剪图片意图
        Intent intent = new Intent("com.android.camera.action.CROP");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            //  大于等于24即为7.0及以上执行内容
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }
        intent.setDataAndType(uri, "image/*");
        intent.putExtra("crop", "true");
        // 裁剪框的比例，1：1
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        // 裁剪后输出图片的尺寸大小
        intent.putExtra("outputX", 250);
        intent.putExtra("outputY", 250);
        intent.putExtra("outputFormat", "JPEG");// 图片格式
        intent.putExtra("noFaceDetection", false);// 取消人脸识别
        intent.putExtra("return-data", true);
        intent.putExtra("circleCrop", false);
        // 开启一个带有返回值的Activity，请求码为PHOTO_REQUEST_CUT
        ((Activity)mContext).startActivityForResult(intent, PHOTO_REQUEST_CUT);
    }
    // 打开相机
    public void openCamera() {
            Uri uri=null;
            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
            if (hasSdcard()) {
                tempFile = new File(Environment.getExternalStorageDirectory(),
                        PHOTO_FILE_NAME);
            }else {
                tempFile = new File(mContext.getCacheDir(),
                        PHOTO_FILE_NAME);
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                //  大于等于24即为7.0及以上执行内容
                uri = FileProvider.getUriForFile(mContext, "com.example.administrator.jim", tempFile);//通过FileProvider创建一个content类型的Uri
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION); //添加这一句表示对目标应用临时授权该Uri
            } else {
                //  低于24即为7.0以下执行内容
                uri = Uri.fromFile(tempFile);
            }
            intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            // 开启一个带有返回值的Activity，请求码为PHOTO_REQUEST_CAREMA
            ((Activity)mContext).startActivityForResult(intent, PHOTO_REQUEST_CAMERA);
    }
}
