package com.example.administrator.jim.bean;

import android.graphics.Bitmap;

import java.io.Serializable;

/**
 * Created by BarBrothers on 2017/12/27.
 */

public class UserInfoBean implements Serializable {
    private Bitmap mBitmap;
    private String nikkname,gexingqianm,username,beizhu,gender,diqu,birthdry;

    public String getNikkname() {
        return nikkname;
    }

    public void setNikkname(String nikkname) {
        this.nikkname = nikkname;
    }

    public String getGexingqianm() {
        return gexingqianm;
    }

    public void setGexingqianm(String gexingqianm) {
        this.gexingqianm = gexingqianm;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getBeizhu() {
        return beizhu;
    }

    public void setBeizhu(String beizhu) {
        this.beizhu = beizhu;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDiqu() {
        return diqu;
    }

    public void setDiqu(String diqu) {
        this.diqu = diqu;
    }

    public String getBirthdry() {
        return birthdry;
    }

    public void setBirthdry(String birthdry) {
        this.birthdry = birthdry;
    }

    public Bitmap getBitmap() {
        return mBitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        mBitmap = bitmap;
    }
}
