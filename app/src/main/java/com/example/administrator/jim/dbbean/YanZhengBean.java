package com.example.administrator.jim.dbbean;

import org.litepal.crud.DataSupport;

/**
 * Created by Administrator on 2017/12/25 0025.
 * Date 2017/12/25 0025
 */
public class YanZhengBean extends DataSupport {
    private String name;
    private String reson;
    private int falg;
    private String localuser;
    private String appk;
    private long updateTime;

    public long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(long updateTime) {
        this.updateTime = updateTime;
    }

    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getReson() {
        return this.reson;
    }
    public void setReson(String reson) {
        this.reson = reson;
    }
    public int getFalg() {
        return this.falg;
    }
    public void setFalg(int falg) {
        this.falg = falg;
    }
    public String getAppk() {
        return this.appk;
    }
    public void setAppk(String appk) {
        this.appk = appk;
    }
    public String getLocaluser() {
        return this.localuser;
    }
    public void setLocaluser(String localuser) {
        this.localuser = localuser;
    }
}
