package com.example.administrator.jim.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Administrator on 2017/12/15 0015.
 * Date 2017/12/15 0015
 */

public class SPUtil {
    public static final int SPUtil_BOOLEAN=0;
    public static final int SPUtil_STRING=1;
    public static final int SPUtil_INT=2;
    public static final String WELCOME_FLAG="WELCOME_FLAG";
    public static final String LOGIN_PASS="LOGIN_PASS";
    public static final String LOGIN_USERNAME="LOGIN_USERNAME";
    public static final String LOGIN_PASSWOED="LOGIN_PASSWOED";
    public static void putValue(Context ctx,String key,int SPUtil_MODE,Object value){
        SharedPreferences sp=ctx.getSharedPreferences("sp_data",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        switch (SPUtil_MODE){
            case SPUtil_BOOLEAN:
                editor.putBoolean(key, (Boolean) value);
                break;
            case SPUtil_STRING:
                editor.putString(key, (String) value);
                break;
            case SPUtil_INT:
                editor.putInt(key, (Integer) value);
                break;
        }
        editor.commit();
    }
    public static Object getValue(Context ctx,String key,int SPUtil_MODE,Object defaultvalue){
        SharedPreferences sp=ctx.getSharedPreferences("sp_data",Context.MODE_PRIVATE);
        switch (SPUtil_MODE){
            case SPUtil_BOOLEAN:
                return sp.getBoolean(key, (Boolean) defaultvalue);
            case SPUtil_STRING:
                return sp.getString(key, (String) defaultvalue);
            case SPUtil_INT:
                return sp.getInt(key, (Integer) defaultvalue);
            default:
                return null;
        }
    }
}
