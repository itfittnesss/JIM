package com.example.administrator.jim.bottomdialog;

import android.view.View;

import com.example.administrator.jim.MainActivity;
import com.example.administrator.jim.R;
import com.example.administrator.jim.view.main.Mine_SettingActivity;
import com.sdsmdg.tastytoast.TastyToast;

import cn.jpush.im.android.api.JMessageClient;
import cn.jpush.im.android.api.model.UserInfo;
import cn.jpush.im.api.BasicCallback;
import me.shaohui.bottomdialog.BaseBottomDialog;

/**
 * Created by shaohui on 2016/12/10.
 */

public class GenderSettingBottomDialog extends BaseBottomDialog implements View.OnClickListener{
    @Override
    public int getLayoutRes() {
        return R.layout.layout_bottom_gender_setting;
    }
    @Override
    public void bindView(final View v) {
        v.findViewById(R.id.layout_bottom_gender_nan).setOnClickListener(this);
        v.findViewById(R.id.layout_bottom_gender_nv).setOnClickListener(this);
        v.findViewById(R.id.layout_bottom_gender_baomi).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.layout_bottom_gender_nan:
                MainActivity.mMyInfo.setGender(UserInfo.Gender.male);
                JMessageClient.updateMyInfo(UserInfo.Field.gender, MainActivity.mMyInfo, new BasicCallback() {
                    @Override
                    public void gotResult(int i, String s) {
                    }
                });
                TastyToast.makeText(getContext(), "修改成功!", TastyToast.LENGTH_SHORT, TastyToast.SUCCESS);
                //调用Activity的更新用户数据方法
                ((Mine_SettingActivity)getContext()).updateUserInfo();
                dismiss();
                break;
            case R.id.layout_bottom_gender_nv:
                MainActivity.mMyInfo.setGender(UserInfo.Gender.female);
                JMessageClient.updateMyInfo(UserInfo.Field.gender, MainActivity.mMyInfo, new BasicCallback() {
                    @Override
                    public void gotResult(int i, String s) {
                    }
                });
                TastyToast.makeText(getContext(), "修改成功!", TastyToast.LENGTH_SHORT, TastyToast.SUCCESS);
                //调用Activity的更新用户数据方法
                ((Mine_SettingActivity)getContext()).updateUserInfo();
                dismiss();
                break;
            case R.id.layout_bottom_gender_baomi:
                MainActivity.mMyInfo.setGender(UserInfo.Gender.unknown);
                JMessageClient.updateMyInfo(UserInfo.Field.gender, MainActivity.mMyInfo, new BasicCallback() {
                    @Override
                    public void gotResult(int i, String s) {
                    }
                });
                TastyToast.makeText(getContext(), "修改成功!", TastyToast.LENGTH_SHORT, TastyToast.SUCCESS);
                //调用Activity的更新用户数据方法
                ((Mine_SettingActivity)getContext()).updateUserInfo();
                dismiss();
                break;
        }
    }
}
