package com.example.administrator.jim.bottomdialog;

import android.graphics.Bitmap;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.administrator.jim.R;
import com.example.administrator.jim.addresslist.AddressListView;
import com.example.administrator.jim.bean.NewFriendBean;
import com.example.administrator.jim.utils.DBUtils;
import com.example.administrator.jim.value.YanZhengValue;
import com.sdsmdg.tastytoast.TastyToast;

import cn.jpush.im.android.api.ContactManager;
import cn.jpush.im.android.api.JMessageClient;
import cn.jpush.im.android.api.callback.GetAvatarBitmapCallback;
import cn.jpush.im.android.api.callback.GetUserInfoCallback;
import cn.jpush.im.android.api.model.UserInfo;
import cn.jpush.im.api.BasicCallback;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import me.shaohui.bottomdialog.BaseBottomDialog;

/**
 * Created by shaohui on 2016/12/10.
 */

public class AddFriendReasonBottomDialog extends BaseBottomDialog implements View.OnClickListener{
    EditText mEditText;
    TextView mTextView;
    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.layout_bottom_nikname_setting;
    }
    @Override
    public void bindView(final View v) {
        mEditText=(EditText)v.findViewById(R.id.edit_text);
        mTextView=(TextView)v.findViewById(R.id.TextView_send);
        v.findViewById(R.id.TextView_send_quxiao).setOnClickListener(this);
        mEditText.setHint("你需要发送验证申请，等待对方同意");
        mTextView.setOnClickListener(this);
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.TextView_send:
                boolean isinfoexits=false;
                for (NewFriendBean n:AddressListView.mdatas){
                    if(n.getUsername().equals(username)&&n.getFlag()==YanZhengValue.YANZHENG_DAIYANZHENG){
                        isinfoexits=true;
                    }
                }
                if(!isinfoexits){
                    ContactManager.sendInvitationRequest(username, null, mEditText.getText().toString().trim(), new BasicCallback() {
                        @Override
                        public void gotResult(int i, String s) {
                            if(i==0){
                                TastyToast.makeText(getContext(), "申请成功!", TastyToast.LENGTH_SHORT, TastyToast.SUCCESS);
                                Observable.create(new ObservableOnSubscribe<NewFriendBean>() {
                                    @Override
                                    public void subscribe(final ObservableEmitter<NewFriendBean> e) throws Exception {
                                        DBUtils.insertYanZhengFriendData(username,"", YanZhengValue.YANZHENG_DAIYANZHENG,"");
                                        final NewFriendBean newFriendBean=new NewFriendBean();
                                        JMessageClient.getUserInfo(username, "", new GetUserInfoCallback() {
                                            @Override
                                            public void gotResult(int i, String s, UserInfo userInfo) {
                                                if(i==0){
                                                    newFriendBean.setUsername(username);
                                                    if(userInfo.getNickname().equals("")){
                                                        newFriendBean.setName(userInfo.getUserName());
                                                    }else {
                                                        newFriendBean.setName(userInfo.getNickname());
                                                    }
                                                    userInfo.getAvatarBitmap(new GetAvatarBitmapCallback() {
                                                        @Override
                                                        public void gotResult(int i, String s, Bitmap bitmap) {
                                                            newFriendBean.setBitmap(bitmap);
                                                            newFriendBean.setFlag(YanZhengValue.YANZHENG_DAIYANZHENG);
                                                            e.onNext(newFriendBean);
                                                        }
                                                    });
                                                }
                                            }
                                        });
                                    }
                                }).subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<NewFriendBean>() {
                                    @Override
                                    public void accept(NewFriendBean newFriendBean) throws Exception {
                                        AddressListView.mdatas.add(0,newFriendBean);
                                        dismiss();
                                    }
                                });
                            }else {
                                TastyToast.makeText(getContext(), "申请失败!", TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                                dismiss();
                            }
                        }
                    });
                }else {
                    dismiss();
                }
                break;
            case R.id.TextView_send_quxiao:
                dismiss();
                break;
        }
    }
}
