package com.example.administrator.jim.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.administrator.jim.R;

/**
 * Created by BarBrothers on 2017/12/16.
 */

public class Mine_View_ListViewAdapter extends BaseAdapter {
    private int imagedatas[];
    private String txtdatas[];
    private Context mContext;
    public Mine_View_ListViewAdapter(Context ctx,int imagedatas[],String txtdatas[]){
        mContext=ctx;
        this.imagedatas=imagedatas;
        this.txtdatas=txtdatas;
    }
    @Override
    public int getCount() {
        return imagedatas.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View inflate = View.inflate(mContext, R.layout.mine_view_item, null);
        ImageView img= (ImageView) inflate.findViewById(R.id.mine_view_image);
        TextView tv= (TextView) inflate.findViewById(R.id.mine_view_txt);
        img.setImageResource(imagedatas[i]);
        tv.setText(txtdatas[i]);
        return inflate;
    }
}
