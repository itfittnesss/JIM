package com.example.administrator.jim.view.main.about;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.example.administrator.jim.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by BarBrothers on 2017/12/17.
 */

public class AboutAutorActivity extends AppCompatActivity {
    @BindView(R.id.aboutautoractivity_ImgView_back)
    ImageView mImageView_back;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_autor);
        getSupportActionBar().hide();
        ButterKnife.bind(this);
    }
    @OnClick(R.id.aboutautoractivity_ImgView_back)
    public void onClick(View v){
        finish();
    }
}
