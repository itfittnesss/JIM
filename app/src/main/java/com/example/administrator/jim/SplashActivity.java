package com.example.administrator.jim;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.example.administrator.jim.loginandregist.LoginActvity;
import com.example.administrator.jim.utils.SPUtil;
import com.ramotion.paperonboarding.PaperOnboardingFragment;
import com.ramotion.paperonboarding.PaperOnboardingPage;
import com.ramotion.paperonboarding.listeners.PaperOnboardingOnRightOutListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.wangyuwei.particleview.ParticleView;

/**
 * Created by Administrator on 2017/12/14 0014.
 * Date 2017/12/14 0014
 */

public class SplashActivity extends AppCompatActivity {
    private Handler mHandler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case 0:
                    if(mWelcomflag){
                        startActivity(new Intent(SplashActivity.this,LoginActvity.class));
                        finish();
                    }else {
                        initWelcom();
                    }
                    break;
                case 1:
                    mParticleView.startAnim();
                    break;
            }
        }
    };
    @BindView(R.id.splash_ParticleView)
    ParticleView mParticleView;
    private Boolean mWelcomflag;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        if(isExsitMianActivity(MainActivity.class)){
            startActivity(new Intent(this,MainActivity.class));
            finish();
        }
        //隐藏标题栏
        getSupportActionBar().hide();
        ButterKnife.bind(this);
        //获取是否已经显示欢迎页
        mWelcomflag = (Boolean) SPUtil.getValue(getApplicationContext(), SPUtil.WELCOME_FLAG, SPUtil.SPUtil_BOOLEAN, false);
        //半秒后显示动画
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(500);
                    mHandler.sendEmptyMessage(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
        //跳转欢迎页面
        mParticleView.setOnParticleAnimListener(new ParticleView.ParticleAnimListener() {
            @Override
            public void onAnimationEnd() {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(1000);
                            mHandler.sendEmptyMessage(0);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
            }
        });
    }

    private boolean isExsitMianActivity(Class<?> cls){
        Intent intent = new Intent(this, cls);
        ComponentName cmpName = intent.resolveActivity(getPackageManager());
        boolean flag = false;
        if (cmpName != null) { // 说明系统中存在这个activity
            ActivityManager am = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
            List<ActivityManager.RunningTaskInfo> taskInfoList = am.getRunningTasks(10);
            for (ActivityManager.RunningTaskInfo taskInfo : taskInfoList) {
                if (taskInfo.baseActivity.equals(cmpName)) { // 说明它已经启动了
                    flag = true;
                    break;  //跳出循环，优化效率
                }
            }
        }
        return flag;
    }
    //欢迎页面
    private void initWelcom() {
        PaperOnboardingPage scr1 = new PaperOnboardingPage("查找好友",
                "    新号码首次登录时，好友名单是空的，要和其它人联系必须先添加好友。成功查找添加好友后，就可以体验DChat的各种特色功能了。",
                Color.parseColor("#678FB4"), R.mipmap.welcom_s, R.drawable.ic_action_search);
        PaperOnboardingPage scr2 = new PaperOnboardingPage("互相交流",
                "    沟通是我们工作、生活的润滑油。沟通更是学习、共享的过程，在交流中可以学习彼此的优点和技巧，提高个人修养，不断完善自我。",
                Color.parseColor("#65B0B4"), R.mipmap.welcom_j, R.drawable.ic_action_user);
        PaperOnboardingPage scr3 = new PaperOnboardingPage("拉近距离",
                "    通过相互交流拉近彼此的距离。",
                Color.parseColor("#9B90BC"), R.mipmap.welcom_l, R.drawable.ic_action_users);

        ArrayList<PaperOnboardingPage> elements = new ArrayList<>();
        elements.add(scr1);
        elements.add(scr2);
        elements.add(scr3);
        PaperOnboardingFragment onBoardingFragment = PaperOnboardingFragment.newInstance(elements);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        //加载到当前布局
        fragmentTransaction.add(android.R.id.content,onBoardingFragment);
        fragmentTransaction.commit();
        onBoardingFragment.setOnRightOutListener(new PaperOnboardingOnRightOutListener() {
            @Override
            public void onRightOut() {
                Intent intent=new Intent(SplashActivity.this,LoginActvity.class);
                startActivity(intent);
                SPUtil.putValue(getApplicationContext(),SPUtil.WELCOME_FLAG,SPUtil.SPUtil_BOOLEAN,true);
                finish();
            }
        });
    }
}
