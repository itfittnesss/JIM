package com.example.administrator.jim.utils;

import android.content.ContentValues;

import com.example.administrator.jim.MainActivity;
import com.example.administrator.jim.dbbean.YanZhengBean;

import org.litepal.crud.DataSupport;

import java.util.List;

/**
 * Created by Administrator on 2017/12/26 0026.
 * Date 2017/12/26 0026
 */

public class DBUtils {

    //验证信息数据库操作
    //添加
    public static boolean insertYanZhengFriendData(String name, String reason, int flag, String appk){
        List<YanZhengBean> isexitslist = DataSupport.where("localuser = ? and name = ?", MainActivity.mMyInfo.getUserName(), name).order("updateTime desc").find(YanZhengBean.class);
        if(isexitslist!=null && isexitslist.size()>0){
            updateYanZhengFriendReason(name,reason);
            return false;
        }
        YanZhengBean yanZhengBean=new YanZhengBean();
        yanZhengBean.setName(name);
        yanZhengBean.setReson(reason);
        yanZhengBean.setFalg(flag);
        yanZhengBean.setAppk(appk);
        yanZhengBean.setLocaluser(MainActivity.mMyInfo.getUserName());
        yanZhengBean.setUpdateTime(System.currentTimeMillis());
        boolean save = yanZhengBean.save();
        return save;
    }
    //查询
    public static List<YanZhengBean> queryYanZhengFriendDatas(){
        return DataSupport.where("localuser = ?", MainActivity.mMyInfo.getUserName()).order("updateTime desc").find(YanZhengBean.class);
    }
    //更新验证标记 已同意，已拒绝。。。。。。
    public static void updateYanZhengFriendFlag(String username,int flag){
        ContentValues values = new ContentValues();
        values.put("falg", flag);
        DataSupport.updateAll(YanZhengBean.class, values,"name = ? and localuser = ?",username,MainActivity.mMyInfo.getUserName());
    }
    //更新验证原因
    private static void updateYanZhengFriendReason(String username,String reason){
        ContentValues values = new ContentValues();
        values.put("reson", reason);
        values.put("updateTime",System.currentTimeMillis());
        DataSupport.updateAll(YanZhengBean.class, values,"name = ? and localuser = ?",username,MainActivity.mMyInfo.getUserName());
    }
}
