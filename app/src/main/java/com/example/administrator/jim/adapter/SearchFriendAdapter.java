package com.example.administrator.jim.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.administrator.jim.R;
import com.example.administrator.jim.bean.Contact;
import com.example.administrator.jim.minterface.SearchFriendListClickListener;
import com.example.administrator.jim.utils.BitmapUtils;

import java.util.List;

import agency.tango.android.avatarview.IImageLoader;
import agency.tango.android.avatarview.loader.PicassoLoader;
import agency.tango.android.avatarview.views.AvatarView;

/**
 * Created by gjz on 9/4/16.
 */
public class SearchFriendAdapter extends RecyclerView.Adapter<SearchFriendAdapter.ContactsViewHolder> {

    private List<Contact> contacts;
    private int layoutId;
    //加载头像工具
    private Context mContext;
    IImageLoader imageLoader;
    private SearchFriendListClickListener mSearchFriendListClickListener;

    public List<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
    }

    public SearchFriendListClickListener getSearchFriendListClickListener() {
        return mSearchFriendListClickListener;
    }

    public void setSearchFriendListClickListener(SearchFriendListClickListener searchFriendListClickListener) {
        mSearchFriendListClickListener = searchFriendListClickListener;
    }

    public SearchFriendAdapter(Context mContext, List<Contact> contacts, int layoutId) {
        this.contacts = contacts;
        this.layoutId = layoutId;
        this.mContext=mContext;
        imageLoader=new PicassoLoader();
    }

    @Override
    public ContactsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = View.inflate(mContext,layoutId,null);
        return new ContactsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ContactsViewHolder holder, final int position) {
        Contact contact = contacts.get(position);
        holder.tvIndex.setVisibility(View.GONE);
        if(contact.getBitmap()==null){
            Glide.with(mContext).load(R.mipmap.aurora_headicon_default).into(holder.ivAvatar);
        }else {
            Glide.with(mContext).load(BitmapUtils.getBitMapBytes(contact.getBitmap())).into(holder.ivAvatar);
        }
        if(contact.getName().equals("")){
            holder.tvName.setText(contact.getUsername());
        }else {
            holder.tvName.setText(contact.getName());
        }
        holder.mrela.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSearchFriendListClickListener.onItemClick(view,position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return contacts.size();
    }

    class ContactsViewHolder extends RecyclerView.ViewHolder {
        public TextView tvIndex;
        public AvatarView ivAvatar;
        public TextView tvName;
        public RelativeLayout mrela;
        public ContactsViewHolder(View itemView) {
            super(itemView);
            tvIndex = (TextView) itemView.findViewById(R.id.tv_index);
            ivAvatar = (AvatarView) itemView.findViewById(R.id.iv_avatar);
            tvName = (TextView) itemView.findViewById(R.id.tv_name);
            mrela= (RelativeLayout) itemView.findViewById(R.id.mrela);
        }
    }
}