package com.example.administrator.jim;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.example.administrator.jim.loginandregist.LoginActvity;
import com.example.administrator.jim.utils.ActivityUtils;
import com.example.administrator.jim.utils.SPUtil;
import com.example.administrator.jim.value.MyValue;

import net.lemonsoft.lemonhello.LemonHello;
import net.lemonsoft.lemonhello.LemonHelloAction;
import net.lemonsoft.lemonhello.LemonHelloInfo;
import net.lemonsoft.lemonhello.LemonHelloView;
import net.lemonsoft.lemonhello.interfaces.LemonHelloActionDelegate;

import cn.jpush.im.android.api.JMessageClient;
import cn.jpush.im.android.api.event.LoginStateChangeEvent;
import cn.jpush.im.api.BasicCallback;

/**
 * Created by BarBrothers on 2017/12/30.
 */

public class BaseActivity extends AppCompatActivity {
    private Class clazz;

    public Class getClazz() {
        return clazz;
    }

    public void setClazz(Class clazz) {
        this.clazz = clazz;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(MyValue.logoutflag==true){
            MyValue.logoutflag=false;
            showLogoutDialog();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(clazz!=MainActivity.class){
            MainActivity.mMyInfo=JMessageClient.getMyInfo();
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        JMessageClient.registerEventReceiver(this);
    }
    public void onEventMainThread(LoginStateChangeEvent event) {
        boolean activityTop = ActivityUtils.isActivityTop(this, clazz);
        if(activityTop){
            if (event.getReason() == LoginStateChangeEvent.Reason.user_logout) {
                showLogoutDialog();
            }
        }else {
            if (event.getReason() == LoginStateChangeEvent.Reason.user_logout) {
                MyValue.logoutflag=true;
            }
        }
    }
    private void showLogoutDialog(){
        LemonHello.getWarningHello("下线通知", "您的账号在其他正在设备登录，如果不是您本人登录请尽快修改密码。")
                .addAction(new LemonHelloAction("退出", new LemonHelloActionDelegate() {
                    @Override
                    public void onClick(LemonHelloView helloView, LemonHelloInfo helloInfo, LemonHelloAction helloAction) {
                        helloView.hide();
                        startActivity(new Intent(BaseActivity.this, LoginActvity.class));
                        MyValue.logoutflag=false;
                        finish();
                    }
                }))
                .addAction(new LemonHelloAction("重新登录", new LemonHelloActionDelegate() {
                    @Override
                    public void onClick(LemonHelloView helloView, LemonHelloInfo helloInfo, LemonHelloAction helloAction) {
                        String username = (String) SPUtil.getValue(getApplicationContext(), SPUtil.LOGIN_USERNAME, SPUtil.SPUtil_STRING, "");
                        String userpassword = (String) SPUtil.getValue(getApplicationContext(), SPUtil.LOGIN_PASSWOED, SPUtil.SPUtil_STRING, "");
                        helloView.hide();
                        MyValue.logoutflag=false;
                        JMessageClient.login(username, userpassword, new BasicCallback() {
                            @Override
                            public void gotResult(int i, String s) {

                            }
                        });
                    }
                }))
                .show(this);
    }
}
