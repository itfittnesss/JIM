package com.example.administrator.jim.adapter;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

/**
 * Created by BarBrothers on 2017/12/16.
 */

public class MainActivity_ViewPagerAdapter extends PagerAdapter {
    private ArrayList<View> mViewDatas;
    public MainActivity_ViewPagerAdapter(ArrayList<View> mViewDatas){
        this.mViewDatas=mViewDatas;
    }
    @Override
    public int getCount() {
        return mViewDatas.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view==object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        container.addView(mViewDatas.get(position));
        return mViewDatas.get(position);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
