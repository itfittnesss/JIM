package com.example.administrator.jim.view.main.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.administrator.jim.R;
import com.example.administrator.jim.imuisample.messages.MessageListActivity;

import java.io.File;
import java.util.List;

import cn.jpush.im.android.api.JMessageClient;
import cn.jpush.im.android.api.content.MessageContent;
import cn.jpush.im.android.api.content.TextContent;
import cn.jpush.im.android.api.model.Conversation;
import cn.jpush.im.android.api.model.UserInfo;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Administrator on 2018/2/1 0001.
 * Date 2018/2/1 0001
 */

public class MessageAdapter extends RecyclerView.Adapter {
    private Context mContext;
    private  List<Conversation> mConversationList;

    public List<Conversation> getConversationList() {
        return mConversationList;
    }

    public void setConversationList(List<Conversation> conversationList) {
        mConversationList = conversationList;
    }

    public MessageAdapter(Context context, List<Conversation> conversationList){
        mConversationList = conversationList;
        mContext=context;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyMessageHolder(View.inflate(mContext,R.layout.item_message,null));
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        MyMessageHolder holder1 = (MyMessageHolder) holder;
        Conversation conversation = mConversationList.get(position);
        holder1.mTextView_username.setText(conversation.getTitle());
        File avatarFile = conversation.getAvatarFile();
        if(avatarFile!=null&&avatarFile.exists()){
            Glide.with(mContext).load(avatarFile).into(holder1.mCircleImageView);
        }else {
            Glide.with(mContext).load(R.mipmap.aurora_headicon_default).into(holder1.mCircleImageView);
        }
        MessageContent content = conversation.getLatestMessage().getContent();
        String LatestMessage="";
        switch (content.getContentType()){
            case text:
                TextContent textContent = (TextContent) content;
                LatestMessage=textContent.getText();
                break;
            case image:
                LatestMessage="[图片]";
                break;
            case video:
                LatestMessage="[视频]";
                break;
            case voice:
                LatestMessage="[声音]";
                break;
            case file:
                LatestMessage="[文件]";
                break;
        }
        holder1.mTextView_LatestMessage.setText(LatestMessage);

        holder1.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mOnSwipeListener) {
                    //如果删除时，不使用mAdapter.notifyItemRemoved(pos)，则删除没有动画效果，
                    //且如果想让侧滑菜单同时关闭，需要同时调用 ((CstSwipeDelMenu) holder.itemView).quickClose();
                    //((CstSwipeDelMenu) holder.itemView).quickClose();
                    Object targetInfo = mConversationList.get(holder.getAdapterPosition()).getTargetInfo();
                    if(targetInfo instanceof UserInfo){
                        JMessageClient.deleteSingleConversation(((UserInfo)targetInfo).getUserName());
                    }
                    mOnSwipeListener.onDel(holder.getAdapterPosition());
                }
            }
        });
        //注意事项，设置item点击，不能对整个holder.itemView设置咯，只能对第一个子View，即原来的content设置，这算是局限性吧。
        (holder1.mRelativeLayout_content).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Object targetInfo = mConversationList.get(holder.getAdapterPosition()).getTargetInfo();
                if(targetInfo instanceof UserInfo){
                    Intent intent = new Intent(mContext, MessageListActivity.class);
                    intent.putExtra("username",((UserInfo)targetInfo).getUserName());
                    mContext.startActivity(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mConversationList.size();
    }
    /**
     * 和Activity通信的接口
     */
    public interface onSwipeListener {
        void onDel(int pos);
    }

    private onSwipeListener mOnSwipeListener;

    public onSwipeListener getOnDelListener() {
        return mOnSwipeListener;
    }

    public void setOnDelListener(onSwipeListener mOnDelListener) {
        this.mOnSwipeListener = mOnDelListener;
    }
    static class MyMessageHolder extends RecyclerView.ViewHolder{
        private CircleImageView mCircleImageView;
        private RelativeLayout mRelativeLayout_content;
        private TextView mTextView_username,mTextView_LatestMessage;
        private RelativeLayout btnDelete;
        public MyMessageHolder(View itemView) {
            super(itemView);
            mCircleImageView= (CircleImageView) itemView.findViewById(R.id.item_message_avtar);
            mTextView_username= (TextView) itemView.findViewById(R.id.item_message_username);
            mTextView_LatestMessage= (TextView) itemView.findViewById(R.id.item_message_LatestMessage);
            btnDelete = (RelativeLayout) itemView.findViewById(R.id.btnDelete);
            mRelativeLayout_content= (RelativeLayout) itemView.findViewById(R.id.item_message_content);
        }
    }
}
