package com.example.administrator.jim.addresslist;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.administrator.jim.BaseActivity;
import com.example.administrator.jim.MainActivity;
import com.example.administrator.jim.R;
import com.example.administrator.jim.bean.UserInfoBean;
import com.example.administrator.jim.bottomdialog.AddFriendReasonBottomDialog;
import com.example.administrator.jim.imuisample.messages.MessageListActivity;
import com.example.administrator.jim.utils.BitmapUtils;

import agency.tango.android.avatarview.IImageLoader;
import agency.tango.android.avatarview.loader.PicassoLoader;
import agency.tango.android.avatarview.views.AvatarView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.jpush.im.android.api.JMessageClient;
import cn.jpush.im.android.api.callback.GetAvatarBitmapCallback;
import cn.jpush.im.android.api.callback.GetUserInfoCallback;
import cn.jpush.im.android.api.model.UserInfo;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by Administrator on 2017/12/28 0028.
 * Date 2017/12/28 0028
 */
//查找好友-》详细信息
public class UserInfoDescActivity extends BaseActivity {
    @BindView(R.id.userinfodesc_item_back)
    ImageView mUserinfodescItemBack;
    @BindView(R.id.userinfodesc_item_avaicon)
    AvatarView mUserinfodescItemAvaicon;
    @BindView(R.id.userinfodesc_item_username)
    TextView mUserinfodescItemUsername;
    @BindView(R.id.userinfodesc_item_nikname)
    TextView mUserinfodescItemNikname;
    @BindView(R.id.userinfodesc_item_gender)
    TextView mUserinfodescItemGender;
    @BindView(R.id.userinfodesc_item_adress)
    TextView mUserinfodescItemAdress;
    @BindView(R.id.userinfodesc_item_bt_add)
    FancyButton mUserinfodescItemBtAdd;
    private IImageLoader imageLoader;
    private UserInfoBean mUserinfo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actvivty_userinfodesc);
        getSupportActionBar().hide();
        setClazz(UserInfoDescActivity.class);
        ButterKnife.bind(this);
        initDatas();
    }
    private void initDatas() {
        imageLoader = new PicassoLoader();
        Intent intent = getIntent();
        boolean isfriend = intent.getBooleanExtra("isfriend", false);
        mUserinfo = (UserInfoBean) intent.getSerializableExtra("userinfo");
        if(isfriend){
            mUserinfodescItemBtAdd.setText("发消息");
        }else {
            mUserinfodescItemBtAdd.setText("加好友");
        }
        if(mUserinfo.getUsername().equals(MainActivity.mMyInfo.getUserName())){
            mUserinfodescItemBtAdd.setVisibility(View.GONE);
        }
        //加载头像
        JMessageClient.getUserInfo(mUserinfo.getUsername(), new GetUserInfoCallback() {
            @Override
            public void gotResult(int i, String s, UserInfo userInfo) {
                if(i==0){
                    userInfo.getBigAvatarBitmap(new GetAvatarBitmapCallback() {
                        @Override
                        public void gotResult(int i, String s, Bitmap bitmap) {
                            if(bitmap!=null){
                                Glide.with(UserInfoDescActivity.this)
                                        .load(BitmapUtils.getBitMapBytes(bitmap))
                                        .into(mUserinfodescItemAvaicon);
                            }else {
                                Glide.with(UserInfoDescActivity.this)
                                        .load(R.mipmap.aurora_headicon_default)
                                        .into(mUserinfodescItemAvaicon);
                            }
                        }
                    });
                }
            }
        });
        mUserinfodescItemAdress.setText(mUserinfo.getDiqu());
        mUserinfodescItemGender.setText(mUserinfo.getGender());
        mUserinfodescItemNikname.setText(mUserinfo.getNikkname());
        mUserinfodescItemUsername.setText(mUserinfo.getUsername());
    }
    @OnClick({R.id.userinfodesc_item_back,R.id.userinfodesc_item_bt_add})
    public void onClick(View v){
        switch (v.getId()){
            case R.id.userinfodesc_item_back:
                finish();
                break;
            case R.id.userinfodesc_item_bt_add:
                String txt = mUserinfodescItemBtAdd.getText().toString().trim();
                if(txt.equals("加好友")){
                    addFriend();
                }else {
                    Intent intent = new Intent(this, MessageListActivity.class);
                    intent.putExtra("username",mUserinfo.getUsername());
                    startActivity(intent);
                }
        }
    }

    private void addFriend() {
        AddFriendReasonBottomDialog addFriendReasonBottomDialog=new AddFriendReasonBottomDialog();
        addFriendReasonBottomDialog.setUsername(mUserinfo.getUsername());
        addFriendReasonBottomDialog.show(getSupportFragmentManager());
    }
}
