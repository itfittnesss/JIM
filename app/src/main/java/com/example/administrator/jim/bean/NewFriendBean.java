package com.example.administrator.jim.bean;

import android.graphics.Bitmap;

/**
 * Created by Administrator on 2017/12/20 0020.
 * Date 2017/12/20 0020
 */

public class NewFriendBean {
    private String name;
    private String res;
    private String username;
    private Bitmap mBitmap;
    private int flag;
    private String appk;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public String getAppk() {
        return appk;
    }

    public void setAppk(String appk) {
        this.appk = appk;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRes() {
        return res;
    }

    public void setRes(String res) {
        this.res = res;
    }

    public Bitmap getBitmap() {
        return mBitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        mBitmap = bitmap;
    }
}
