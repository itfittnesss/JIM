package com.example.administrator.jim.imuisample.messages;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.blankj.utilcode.util.BarUtils;
import com.bumptech.glide.Glide;
import com.example.administrator.jim.BaseActivity;
import com.example.administrator.jim.R;
import com.example.administrator.jim.value.MyValue;

import java.io.File;

/**
 * Created by Administrator on 2018/2/5 0005.
 * Date 2018/2/5 0005
 */

public class ImageActivity extends BaseActivity {
    private ImageView mImageView;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);
        setClazz(ImageActivity.class);
        BarUtils.setStatusBarVisibility(this,false);
        getSupportActionBar().hide();
        mImageView= (ImageView) findViewById(R.id.activity_image_img);
        initImage();
    }
    private void initImage() {
        try {
            Glide.with(this).load(new File(getIntent().getStringExtra(MyValue.PHOTOFILEPATH))).into(mImageView);
        }catch (Exception e){
        }
    }
}
