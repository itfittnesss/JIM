package com.example.administrator.jim.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.administrator.jim.R;
import com.example.administrator.jim.addresslist.AddressListView;
import com.example.administrator.jim.bean.Contact;
import com.example.administrator.jim.bean.NewFriendBean;
import com.example.administrator.jim.utils.DBUtils;
import com.example.administrator.jim.value.YanZhengValue;
import com.github.promeg.pinyinhelper.Pinyin;

import java.io.ByteArrayOutputStream;
import java.util.Collections;
import java.util.List;

import agency.tango.android.avatarview.IImageLoader;
import agency.tango.android.avatarview.loader.PicassoLoader;
import agency.tango.android.avatarview.views.AvatarView;
import cn.jpush.im.android.api.ContactManager;
import cn.jpush.im.api.BasicCallback;

/**
 * Created by gjz on 9/4/16.
 */
public class NewFriendAdapter extends RecyclerView.Adapter<NewFriendAdapter.ContactsViewHolder> {

    private List<NewFriendBean> contacts;
    private int layoutId;
    private Context mContext;
    //加载头像工具
    IImageLoader imageLoader;
    private ByteArrayOutputStream mStream;

    public NewFriendAdapter(Context mContext,List<NewFriendBean> contacts, int layoutId) {
        this.contacts = contacts;
        this.layoutId = layoutId;
        this.mContext=mContext;
        imageLoader=new PicassoLoader();
        mStream = new ByteArrayOutputStream();
    }

    @Override
    public ContactsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(layoutId, null);
        return new ContactsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ContactsViewHolder holder, final int position) {
        final NewFriendBean contact = contacts.get(position);
        if(contact.getBitmap()==null){
            imageLoader.loadImage(holder.ivAvatar,null, contact.getUsername(), 50);
        }else {
            contact.getBitmap().compress(Bitmap.CompressFormat.PNG, 100, mStream);
            Glide.with(mContext).load(mStream.toByteArray()).into( holder.ivAvatar);
        }
        switch (contact.getFlag()){
            case YanZhengValue.YANZHENG_NEWFRIEND:

                break;
            case YanZhengValue.YANZHENG_DAIYANZHENG:
                holder.bt_ok.setVisibility(View.GONE);
                holder.bt_no.setVisibility(View.GONE);
                holder.tv_type.setText("待验证");
                holder.tv_type.setVisibility(View.VISIBLE);
                break;
            case YanZhengValue.YANZHENG_JIESHOU:
                holder.bt_ok.setVisibility(View.GONE);
                holder.bt_no.setVisibility(View.GONE);
                holder.tv_type.setText("已同意");
                holder.tv_type.setVisibility(View.VISIBLE);
                break;
            case YanZhengValue.YANZHENG_JUJUE:
                holder.bt_ok.setVisibility(View.GONE);
                holder.bt_no.setVisibility(View.GONE);
                holder.tv_type.setText("已拒绝");
                holder.tv_type.setVisibility(View.VISIBLE);
                break;
        }
        holder.tv_res.setText(contact.getRes());
        holder.tvName.setText(contact.getName());
        mStream.reset();
        holder.bt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //同意好友申请
                ContactManager.acceptInvitation(contact.getUsername(), contact.getAppk(), new BasicCallback() {
                    @Override
                    public void gotResult(int i, String s) {
                        if(i==0){
                            holder.bt_ok.setVisibility(View.GONE);
                            holder.bt_no.setVisibility(View.GONE);
                            holder.tv_type.setText("已同意");
                            holder.tv_type.setVisibility(View.VISIBLE);
                            AddressListView.mdatas.get(position).setFlag(YanZhengValue.YANZHENG_JIESHOU);
                            DBUtils.updateYanZhengFriendFlag(contact.getUsername(),YanZhengValue.YANZHENG_JIESHOU);
                            //加入到通讯录
                            Contact c;
                            if(!contact.getName().equals("")){
                                if(Pinyin.isChinese(contact.getName().charAt(0))){
                                    c=new Contact( Pinyin.toPinyin(contact.getName().charAt(0)).substring(0,1).toUpperCase(),contact.getName());
                                }else {
                                    c=new Contact(contact.getUsername().substring(0,1).toUpperCase(),contact.getUsername());
                                }
                            }else {
                                c=new Contact(contact.getUsername().substring(0,1).toUpperCase(),contact.getUsername());
                            }
                            c.setUsername(contact.getUsername());
                            c.setBitmap(contact.getBitmap());
                            AddressListView.contacts.add(c);
                            Collections.sort(AddressListView.contacts);
                            AddressListView.mContactsAdapter.notifyDataSetChanged();
                        }
                    }
                });
            }
        });
        holder.bt_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //拒绝好友申请
                ContactManager.declineInvitation(contact.getUsername(), contact.getAppk(), "",new BasicCallback() {
                    @Override
                    public void gotResult(int i, String s) {
                        Toast.makeText(mContext, contact.getUsername()+"===="+contact.getAppk(), Toast.LENGTH_SHORT).show();
                        if(i==0){
                            holder.bt_ok.setVisibility(View.GONE);
                            holder.bt_no.setVisibility(View.GONE);
                            holder.tv_type.setText("已拒绝");
                            holder.tv_type.setVisibility(View.VISIBLE);
                            AddressListView.mdatas.get(position).setFlag(YanZhengValue.YANZHENG_JUJUE);
                            DBUtils.updateYanZhengFriendFlag(contact.getUsername(),YanZhengValue.YANZHENG_JUJUE);
                        }
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return contacts.size();
    }

    class ContactsViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_res;
        public AvatarView ivAvatar;
        public TextView tvName,tv_type;
        public Button bt_ok,bt_no;


        public ContactsViewHolder(View itemView) {
            super(itemView);
            tv_res = (TextView) itemView.findViewById(R.id.tv_res);
            ivAvatar = (AvatarView) itemView.findViewById(R.id.iv_avatar);
            tvName = (TextView) itemView.findViewById(R.id.tv_name);
            bt_ok= (Button) itemView.findViewById(R.id.bt_ok);
            bt_no= (Button) itemView.findViewById(R.id.bt_no);
            tv_type=(TextView) itemView.findViewById(R.id.tv_type);
        }
    }
}