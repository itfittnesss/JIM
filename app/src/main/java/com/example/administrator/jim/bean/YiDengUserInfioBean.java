package com.example.administrator.jim.bean;

import java.util.List;

/**
 * Created by BarBrothers on 2017/12/30.
 */

public class YiDengUserInfioBean {

    /**
     * phone : 15254546047
     * name : 完颜无泪
     * email :
     * avatar : https://dn-spapi1.qbox.me/avatar/2017/12/30/icvvkpq4iu4br4yl.jpg
     * regioncode : 86
     * persona : {"gender":"male","tags":["goodLooking","eyeglasses"],"location":{"country":"CN","province":"山东","city":"烟台"},"generation":"10s","character":""}
     * group_uid : 04e199abd53bfea466aa2c9b1d600a8f
     */

    private String phone;
    private String name;
    private String email;
    private String avatar;
    private String regioncode;
    private PersonaBean persona;
    private String group_uid;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getRegioncode() {
        return regioncode;
    }

    public void setRegioncode(String regioncode) {
        this.regioncode = regioncode;
    }

    public PersonaBean getPersona() {
        return persona;
    }

    public void setPersona(PersonaBean persona) {
        this.persona = persona;
    }

    public String getGroup_uid() {
        return group_uid;
    }

    public void setGroup_uid(String group_uid) {
        this.group_uid = group_uid;
    }

    public static class PersonaBean {
        /**
         * gender : male
         * tags : ["goodLooking","eyeglasses"]
         * location : {"country":"CN","province":"山东","city":"烟台"}
         * generation : 10s
         * character :
         */

        private String gender;
        private LocationBean location;
        private String generation;
        private String character;
        private List<String> tags;

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public LocationBean getLocation() {
            return location;
        }

        public void setLocation(LocationBean location) {
            this.location = location;
        }

        public String getGeneration() {
            return generation;
        }

        public void setGeneration(String generation) {
            this.generation = generation;
        }

        public String getCharacter() {
            return character;
        }

        public void setCharacter(String character) {
            this.character = character;
        }

        public List<String> getTags() {
            return tags;
        }

        public void setTags(List<String> tags) {
            this.tags = tags;
        }

        public static class LocationBean {
            /**
             * country : CN
             * province : 山东
             * city : 烟台
             */

            private String country;
            private String province;
            private String city;

            public String getCountry() {
                return country;
            }

            public void setCountry(String country) {
                this.country = country;
            }

            public String getProvince() {
                return province;
            }

            public void setProvince(String province) {
                this.province = province;
            }

            public String getCity() {
                return city;
            }

            public void setCity(String city) {
                this.city = city;
            }
        }
    }
}
