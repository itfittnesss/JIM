package com.example.administrator.jim.loginandregist;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.example.administrator.jim.R;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.sdsmdg.tastytoast.TastyToast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.jpush.im.android.api.JMessageClient;
import cn.jpush.im.android.api.options.RegisterOptionalUserInfo;
import cn.jpush.im.api.BasicCallback;
import mehdi.sakout.fancybuttons.FancyButton;


/**
 * Created by Administrator on 2017/12/15 0015.
 * Date 2017/12/15 0015
 */

public class RegisteActivity extends AppCompatActivity {
    @BindView(R.id.registactivity_Editext_nikename)
    MaterialEditText mMaterialEditText_nikename;
    @BindView(R.id.registactivity_Editext_username)
    MaterialEditText mMaterialEditText_username;
    @BindView(R.id.registactivity_Editext_userpass)
    MaterialEditText mMaterialEditText_userpass;
    @BindView(R.id.registactivity_Editext_userpassisok)
    MaterialEditText mMaterialEditText_userpassisok;
    @BindView(R.id.registactivity_Button_regist)
    FancyButton mFancyButton_login;
    @BindView(R.id.registactivity_ImgView_back)
    ImageView mimg_back;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regist);
        ButterKnife.bind(this);
        getSupportActionBar().hide();
    }
    //注册逻辑
    @OnClick({R.id.registactivity_Button_regist,R.id.registactivity_ImgView_back})
    public void onClick(View v){
        switch (v.getId()){
            case R.id.registactivity_Button_regist:
                String userName=mMaterialEditText_username.getText().toString().trim();
                String mpassword=mMaterialEditText_userpass.getText().toString().trim();
                String mpasswordisok=mMaterialEditText_userpassisok.getText().toString().trim();
                String nikname=mMaterialEditText_nikename.getText().toString().trim();
                if(userName.equals("")||mpassword.equals("")){
                    TastyToast.makeText(getApplicationContext(), "用户名或密码不能为空", TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                }else if(userName.length()<4||mpassword.length()<4){
                    TastyToast.makeText(getApplicationContext(), "用户名或密码的长度必须为4~12个字符", TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                }else {
                    if(mpasswordisok.equals("")){
                        TastyToast.makeText(getApplicationContext(), "请确认密码", TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                    }else if(!mpassword.equals(mpasswordisok)){
                        TastyToast.makeText(getApplicationContext(), "两次密码不一致", TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                    }else{
                        RegisterOptionalUserInfo registerOptionalUserInfo=new RegisterOptionalUserInfo();
                        if(nikname.equals("")){
                            registerOptionalUserInfo.setNickname(userName);
                        }else {
                            registerOptionalUserInfo.setNickname(nikname);
                        }
                        JMessageClient.register(userName, mpassword, registerOptionalUserInfo, new BasicCallback() {
                            @Override
                            public void gotResult(int i, String s) {
                                switch (i){
                                    case 0:
                                        TastyToast.makeText(getApplicationContext(), "注册成功", TastyToast.LENGTH_SHORT, TastyToast.SUCCESS);
                                        finish();
                                        break;
                                    case 898001:
                                        TastyToast.makeText(getApplicationContext(), "注册失败(用户名已存在)", TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                                        break;
                                    default:
                                        TastyToast.makeText(getApplicationContext(), "注册失败(用户名只能是字母和数字)", TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                                        break;
                                }
                            }
                        });
                    }
                }
                break;
            case R.id.registactivity_ImgView_back:
                finish();
                break;
        }

    }
}
