package com.example.administrator.jim.view.main;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.administrator.jim.MainActivity;
import com.example.administrator.jim.R;
import com.example.administrator.jim.adapter.Mine_View_ListViewAdapter;
import com.example.administrator.jim.loginandregist.LoginActvity;
import com.example.administrator.jim.share.ShareBottomDialog;
import com.example.administrator.jim.view.main.about.AboutActivity;
import com.example.administrator.jim.view.main.about.AboutAutorActivity;
import com.sdsmdg.tastytoast.TastyToast;

import net.lemonsoft.lemonbubble.LemonBubble;
import net.lemonsoft.lemonbubble.enums.LemonBubbleLayoutStyle;
import net.lemonsoft.lemonbubble.enums.LemonBubbleLocationStyle;
import net.lemonsoft.lemonhello.LemonHello;
import net.lemonsoft.lemonhello.LemonHelloAction;
import net.lemonsoft.lemonhello.LemonHelloInfo;
import net.lemonsoft.lemonhello.LemonHelloView;
import net.lemonsoft.lemonhello.interfaces.LemonHelloActionDelegate;

import java.io.ByteArrayOutputStream;

import agency.tango.android.avatarview.IImageLoader;
import agency.tango.android.avatarview.loader.PicassoLoader;
import agency.tango.android.avatarview.views.AvatarView;
import butterknife.BindView;
import butterknife.ButterKnife;
import cn.jpush.im.android.api.JMessageClient;
import cn.jpush.im.android.api.callback.GetAvatarBitmapCallback;

/**
 * Created by BarBrothers on 2017/12/16.
 */

public class Mine_view extends LinearLayout {
    private View mView;
    private Context mContext;
    IImageLoader imageLoader;
    @BindView(R.id.mine_view_avaimageview)
    AvatarView avatarView;
    @BindView(R.id.view_mine_listview)
    ListView mlistView;
    @BindView(R.id.mine_view_username)
    TextView mtextview_username;
    @BindView(R.id.mine_view_signature)
    TextView mTextView_signature;
    private ByteArrayOutputStream mBaos;

    public Mine_view(Context context) {
        this(context,null);
    }

    public Mine_view(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs,0);
    }

    public Mine_view(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext=context;
//        mView= LayoutInflater.from(mContext).inflate(R.layout.view_mine, this, true);
        mView=View.inflate(context, R.layout.view_mine, this);
        ButterKnife.bind(mView);
        initDatas();
        initListener();
    }
    private void initListener() {
        mlistView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i){
                    case 0:
                        mContext.startActivity(new Intent(mContext, AboutAutorActivity.class));
                        break;
                    case 1:
                        mContext.startActivity(new Intent(mContext, AboutActivity.class));
                        break;
                    case 2:
                        ShareBottomDialog dialog = new ShareBottomDialog();
                        dialog.show(((MainActivity)mContext).getSupportFragmentManager());
                        break;
                    case 3:
                        //注销当前用户
                        LemonHello.getInformationHello("您确定要注销吗？", "注销登录后您将无法接收到当前用户的所有推送消息。")
                                .addAction(new LemonHelloAction("取消", new LemonHelloActionDelegate() {
                                    @Override
                                    public void onClick(LemonHelloView helloView, LemonHelloInfo helloInfo, LemonHelloAction helloAction) {
                                        helloView.hide();
                                    }
                                }))
                                .addAction(new LemonHelloAction("我要注销", Color.RED, new LemonHelloActionDelegate() {
                                    @Override
                                    public void onClick(LemonHelloView helloView, LemonHelloInfo helloInfo, LemonHelloAction helloAction) {
                                        JMessageClient.logout();
                                        helloView.hide();
                                        // 提示框使用了LemonBubble，请您参考：https://github.com/1em0nsOft/LemonBubble4Android
                                        LemonBubble.getRoundProgressBubbleInfo()
                                                .setLocationStyle(LemonBubbleLocationStyle.BOTTOM)
                                                .setLayoutStyle(LemonBubbleLayoutStyle.ICON_LEFT_TITLE_RIGHT)
                                                .setBubbleSize(200, 50)
                                                .setProportionOfDeviation(0.1f)
                                                .setTitle("正在请求服务器...")
                                                .show(mContext);
                                        new Handler().postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                LemonBubble.hide();
                                                mContext.startActivity(new Intent(mContext, LoginActvity.class));
                                                ((MainActivity)mContext).finish();
                                                TastyToast.makeText(mContext, "注销成功期待您下次登录!", TastyToast.LENGTH_SHORT, TastyToast.SUCCESS);
                                            }
                                        }, 1000);
                                    }
                                }))
                                .show(mContext);
                        break;
                    case 4:
                        mContext.startActivity(new Intent(mContext,MyTwoCodeActivity.class));
                        break;
                }
            }
        });
    }

    private void initDatas() {
        mlistView.setAdapter(new Mine_View_ListViewAdapter(mContext,new int[]{R.mipmap.mine_fankui,R.mipmap.mine_about,R.mipmap.mine_fenxiang,R.mipmap.mine_singout, R.mipmap.erwe},new String[]{"反馈","关于","分享","注销","二维码"}));
        imageLoader = new PicassoLoader();
        initUserinfo();
    }
    //更新用户数据
    public void updateUserinfo(){
        initUserinfo();
    }
    public void updateNikname(){
        if(MainActivity.mMyInfo.getNickname().equals("")){
            mtextview_username.setText(MainActivity.mMyInfo.getUserName());
        }else {
            mtextview_username.setText(MainActivity.mMyInfo.getNickname());
        }
    }
    private void initUserinfo(){
        if(MainActivity.mMyInfo!=null){
            MainActivity.mMyInfo.getBigAvatarBitmap(new GetAvatarBitmapCallback() {
                @Override
                public void gotResult(int i, String s, Bitmap bitmap) {
                    if(i==0){
                        if(bitmap!=null){
                            mBaos = new ByteArrayOutputStream();
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, mBaos);
                            byte[] bytes= mBaos.toByteArray();
                            Glide.with(mContext)
                                    .load(bytes)
                                    .into(avatarView);
                        }else{
                            Glide.with(mContext).load(R.mipmap.aurora_headicon_default).into(avatarView);
                        }
                    }else {
                        Glide.with(mContext).load(R.mipmap.aurora_headicon_default).into(avatarView);
                    }
                }
            });
            if(MainActivity.mMyInfo.getNickname().equals("")){
                mtextview_username.setText(MainActivity.mMyInfo.getUserName());
            }else {
                mtextview_username.setText(MainActivity.mMyInfo.getNickname());
            }
            //个性签名
            mTextView_signature.setText(MainActivity.mMyInfo.getSignature());
        }else {
            Glide.with(mContext).load(R.mipmap.aurora_headicon_default).into(avatarView);
        }
    }
}
