package com.example.administrator.jim.minterface;

import android.view.View;

import q.rorbin.badgeview.Badge;

/**
 * Created by Administrator on 2017/12/20 0020.
 * Date 2017/12/20 0020
 */

public interface AddressListClickListener {
    void onItemClick(View v, Badge badge, int pos);
}
