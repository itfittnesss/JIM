package com.example.administrator.jim.imuisample.models;

import android.graphics.Bitmap;

import cn.jiguang.imui.commons.models.IUser;


public class DefaultUser implements IUser {

    private String id;
    private String displayName;
    private Bitmap avatar;

    public DefaultUser(String id, String displayName, Bitmap avatar) {
        this.id = id;
        this.displayName = displayName;
        this.avatar = avatar;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getDisplayName() {
        return displayName;
    }

    @Override
    public Bitmap getAvatarFilePath() {
        return avatar;
    }
}
