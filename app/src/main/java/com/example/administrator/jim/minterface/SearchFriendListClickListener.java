package com.example.administrator.jim.minterface;

import android.view.View;

/**
 * Created by Administrator on 2017/12/20 0020.
 * Date 2017/12/20 0020
 */

public interface SearchFriendListClickListener {
    void onItemClick(View v, int pos);
}
