package com.example.administrator.jim.view.main.about;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.example.administrator.jim.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by BarBrothers on 2017/12/17.
 */

public class AboutActivity extends AppCompatActivity {
    @BindView(R.id.aboutactivity_ImgView_back)
    ImageView mImageView_back;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        getSupportActionBar().hide();
        ButterKnife.bind(this);

    }
    @OnClick({R.id.aboutactivity_ImgView_back,R.id.aboutactivity_item_aboutautor})
    public void onClick(View v){
        switch (v.getId()){
            case R.id.aboutactivity_ImgView_back:
                finish();
                break;
            case R.id.aboutactivity_item_aboutautor:
                startActivity(new Intent(this,AboutAutorActivity.class));
                break;
        }
    }
}
