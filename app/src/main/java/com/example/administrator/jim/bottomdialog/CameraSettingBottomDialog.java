package com.example.administrator.jim.bottomdialog;

import android.view.View;

import com.example.administrator.jim.R;
import com.example.administrator.jim.utils.CameraUtils;

import me.shaohui.bottomdialog.BaseBottomDialog;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by shaohui on 2016/12/10.
 */

public class CameraSettingBottomDialog extends BaseBottomDialog implements View.OnClickListener{
    FancyButton mFancyButton_camera;
    FancyButton mFancyButton_photo;
    FancyButton mFancyButton_Cancel;
    private CameraUtils mCameraUtils;
    public void setCameraUtils(CameraUtils mCameraUtils){
        this.mCameraUtils=mCameraUtils;
    }
    @Override
    public int getLayoutRes() {
        return R.layout.layout_bottom_gender_setting;
    }
    @Override
    public void bindView(final View v) {
        if(mCameraUtils!=null){
            mCameraUtils.checkpermissiom();
        }
        mFancyButton_camera= (FancyButton) v.findViewById(R.id.layout_bottom_gender_nan);
        mFancyButton_photo= (FancyButton) v.findViewById(R.id.layout_bottom_gender_nv);
        mFancyButton_Cancel= (FancyButton) v.findViewById(R.id.layout_bottom_gender_baomi);
        mFancyButton_camera.setText("拍照");
        mFancyButton_photo.setText("相册");
        mFancyButton_Cancel.setText("取消");
        mFancyButton_camera.setOnClickListener(this);
        mFancyButton_photo.setOnClickListener(this);
        mFancyButton_Cancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.layout_bottom_gender_nan:
                mCameraUtils.openCamera();
                dismiss();
                break;
            case R.id.layout_bottom_gender_nv:
                mCameraUtils.openGallery();
                dismiss();
                break;
            case R.id.layout_bottom_gender_baomi:
                dismiss();
                break;
        }
    }
}
