package com.example.administrator.jim.application;

import android.os.StrictMode;

import com.isnc.facesdk.SuperID;

import org.litepal.LitePalApplication;
import org.xutils.x;

import cn.jiguang.imui.BuildConfig;
import cn.jpush.im.android.api.JMessageClient;
import me.shaohui.shareutil.ShareConfig;
import me.shaohui.shareutil.ShareManager;

/**
 * Created by Administrator on 2017/12/14 0014.
 * Date 2017/12/14 0014
 */

public class MyApplication extends LitePalApplication {
    @Override
    public void onCreate() {
        super.onCreate();
        //极光
        JMessageClient.setDebugMode(true);
        JMessageClient.init(this);
        ShareConfig config = ShareConfig.instance()
                .qqId("101445233");
//                .wxId(WX_ID)
//                .weiboId(WEIBO_ID)
                // 下面两个，如果不需要登录功能，可不填写
//                .weiboRedirectUrl(REDIRECT_URL)
//                .wxSecret(WX_ID);
        ShareManager.init(config);
        //一登
        SuperID.initFaceSDK(this);
        //xutils
        x.Ext.init(this);
        if (BuildConfig.DEBUG) {
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                    .detectAll()
                    .penaltyLog()
                    .build());

            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                    .detectAll()
                    .penaltyLog()
                    .build());
        }
    }
}
