package com.example.administrator.jim.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.administrator.jim.R;
import com.example.administrator.jim.bean.Contact;
import com.example.administrator.jim.minterface.AddressListClickListener;
import com.example.administrator.jim.utils.BitmapUtils;

import java.util.List;

import agency.tango.android.avatarview.IImageLoader;
import agency.tango.android.avatarview.loader.PicassoLoader;
import agency.tango.android.avatarview.views.AvatarView;
import q.rorbin.badgeview.Badge;
import q.rorbin.badgeview.QBadgeView;

/**
 * Created by gjz on 9/4/16.
 */
public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.ContactsViewHolder> {

    private List<Contact> contacts;
    private int layoutId;
    //加载头像工具
    private Context mContext;
    IImageLoader imageLoader;
    private AddressListClickListener mAddressListClickListener;

    public AddressListClickListener getAddressListClickListener() {
        return mAddressListClickListener;
    }

    public void setAddressListClickListener(AddressListClickListener addressListClickListener) {
        mAddressListClickListener = addressListClickListener;
    }

    public ContactsAdapter(Context mContext,List<Contact> contacts, int layoutId) {
        this.contacts = contacts;
        this.layoutId = layoutId;
        this.mContext=mContext;
        imageLoader=new PicassoLoader();
    }

    @Override
    public ContactsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = View.inflate(mContext,layoutId,null);
        return new ContactsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ContactsViewHolder holder, final int position) {
        final Contact contact = contacts.get(position);
        if(position==0){
            holder.tvIndex.setVisibility(View.GONE);
            holder.ivAvatar.setImageResource(R.mipmap.message_yanzheng);
        }else if(position==1){
            holder.tvIndex.setVisibility(View.GONE);
            holder.ivAvatar.setImageResource(R.mipmap.message_group);
        }else if (position == 2 || !contacts.get(position-1).getIndex().equals(contact.getIndex())) {
            holder.tvIndex.setVisibility(View.VISIBLE);
            holder.tvIndex.setText(contact.getIndex());
            if(contact.getBitmap()==null){
                Glide.with(mContext).load(R.mipmap.aurora_headicon_default).into(holder.ivAvatar);
            }else {
                Glide.with(mContext).load(BitmapUtils.getBitMapBytes(contact.getBitmap())).into(holder.ivAvatar);
            }
        } else {
            holder.tvIndex.setVisibility(View.GONE);
            if(contact.getBitmap()==null){
                Glide.with(mContext).load(R.mipmap.aurora_headicon_default).into(holder.ivAvatar);
            }else {
                Glide.with(mContext).load(BitmapUtils.getBitMapBytes(contact.getBitmap())).into(holder.ivAvatar);
            }
        }
        holder.badge.setBadgeNumber(contact.getMessageflag());
        holder.badge.setOnDragStateChangedListener(new Badge.OnDragStateChangedListener() {
            @Override
            public void onDragStateChanged(int dragState, Badge badge, View targetView) {
                if (dragState == STATE_SUCCEED) {
                    contact.setMessageflag(0);
                    //拖出去
                }
            }
        });
        if(contact.getName().equals("")){
            holder.tvName.setText(contact.getUsername());
        }else {
            holder.tvName.setText(contact.getName());
        }
        holder.mrela.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAddressListClickListener.onItemClick(view,holder.badge,position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return contacts.size();
    }

    class ContactsViewHolder extends RecyclerView.ViewHolder {
        public TextView tvIndex;
        public AvatarView ivAvatar;
        public TextView tvName;
        public RelativeLayout mrela;
        public  Badge badge;

        public ContactsViewHolder(View itemView) {
            super(itemView);
            tvIndex = (TextView) itemView.findViewById(R.id.tv_index);
            ivAvatar = (AvatarView) itemView.findViewById(R.id.iv_avatar);
            tvName = (TextView) itemView.findViewById(R.id.tv_name);
            mrela= (RelativeLayout) itemView.findViewById(R.id.mrela);
            badge = new QBadgeView(mContext).bindTarget(ivAvatar);
            badge.setBadgeGravity(Gravity.END | Gravity.TOP);
            badge.setBadgeTextSize(14, true);
            badge.setBadgePadding(6, true);
        }
    }
}