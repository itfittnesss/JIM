package cn.jiguang.imui.commons;


import android.graphics.Bitmap;
import android.widget.ImageView;

public interface ImageLoader {

    /**
     * Load image into avatar's ImageView.
     * @param avatarImageView Avatar's ImageView.
     * @param bitmap A file path, or a uri or url.
     */
    void loadAvatarImage(ImageView avatarImageView, Bitmap bitmap);

    /**
     * Load image into image message's ImageView.
     * @param imageView Image message or video message's ImageView.
     * @param string A file path, or a uri or url.
     */
    void loadImage(ImageView imageView, String string);
}
